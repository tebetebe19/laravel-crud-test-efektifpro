<div class="modal fade" id="modalBrand" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pengaturan Brand</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Jenis Barang</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                      <option>Jenis 1</option>
                      <option>Jenis 2</option>
                      <option>Jenis 3</option>
                      <option>Jenis 4</option>
                      <option>Jenis 5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="text1">Brand</label>
                    <input type="text" class="form-control" id="text1">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>