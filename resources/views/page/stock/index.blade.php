@extends('layout.main')

@section('content')
    
<div class="main-content">
    <section class="section">
    <div class="section-header">
        <h1>Stock</h1>
    </div>

    <div class="section-body">
        

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modalStock">
                            Tambah Stock
                        </button>
                    </div>
                    <div class="card-body text-center">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Jenis Barang</th>
                                    <th scope="col">Brand</th>
                                    <th scope="col">Nama Barang</th>
                                    <th scope="col">Stock</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Minyak Goreng</td>
                                    <td>Palmina</td>
                                    <td>Family Pack Series</td>
                                    <td>50</td>
                                    <td>
                                        <button class="btn btn-warning" data-toggle="modal" data-target="#modalStock">Edit</button>
                                        <button class="btn btn-danger">Hapus</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>

@include('page.stock.modal')

@endsection

@section('js')
    
@endsection