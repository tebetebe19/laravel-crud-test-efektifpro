<div class="modal fade" id="modalStock" tabindex="-1" role="dialog" aria-labelledby="modalStockLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalStockLabel">Pengaturan Stock</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Jenis Barang</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                      <option>jenis 1</option>
                      <option>jenis 2</option>
                      <option>jenis 3</option>
                      <option>jenis 4</option>
                      <option>jenis 5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Brand</label>
                    <select class="form-control" id="exampleFormControlSelect2">
                      <option>Brand 1</option>
                      <option>Brand 2</option>
                      <option>Brand 3</option>
                      <option>Brand 4</option>
                      <option>Brand 5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="text1">Nama Barang</label>
                    <input type="text" class="form-control" id="text1">
                </div>
                <div class="form-group">
                    <label for="number1">Stock</label>
                    <input type="Number" class="form-control" id="number1">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>